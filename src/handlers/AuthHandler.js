import UserModel from '../models/UserModel';

class AuthHandler {
  async getUserByEmail(email) {
    // check email lock
    const user = await UserModel.findOne({ email });
    return user;
  }

  createNewUser(data) {
    return UserModel.create(data);
  }

  getPayloadJwtSchema(user) {
    return {
      userId: user._id,
      role: user.role,
    };
  }

  async getUserById(userId) {
    // check email lock
    const user = await UserModel.findOne({ _id: userId });
    return user;
  }
}

export default new AuthHandler();
