import mongoose, { Schema } from 'mongoose';

import { hashText, compareTextAndHash } from '../utils/bcrypt';


const UserSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
  },
});

async function preSaveUserModel(cb) {
  const user = this;
  if (!user.isModified('password')) return cb();
  try {
    const hash = await hashText(user.password);
    user.password = hash;
    return cb();
  } catch (error) {
    return cb(error);
  }
}

async function comparePasswordMethod(candidatePassword) {
  const isMatch = await compareTextAndHash(candidatePassword, this.password);
  return isMatch;
}

UserSchema.pre('save', preSaveUserModel);
UserSchema.methods.comparePassword = comparePasswordMethod;

export default mongoose.model('User', UserSchema);
