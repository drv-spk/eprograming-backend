import http from 'http';
import BootMongo from './boots/BootMongo';
import BootExpress from './boots/BootExpress';
import { PORT } from './config/Server';

const runApp = async () => {
  try {
    await BootMongo();
    // Run services

    const app = BootExpress();

    const server = http.createServer(app);

    server.listen(PORT, () => console.log(`Listening on port ${PORT}`));
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
};

runApp();
