import Base from './BaseService';

class ContactService extends Base {
  constructor() {
    super('Contact');
  }

  async sendEmail({ to, subject, html }) {
    try {
      const data = await this.fetch.post('/contact/sendemail', {
        to,
        subject,
        html,
        keySecret: 'abc',
      });
      return data;
    } catch (error) {
      return false;
    }
  }

  async pushNotification({ notification, userId }) {
    try {
      const data = await this.fetch.post('/notification/pushnotificationnow', {
        notification,
        userId,
        keySecret: 'abc',
      });
      return data;
    } catch (error) {
      return false;
    }
  }

  async saveFcmToken({ userId, fcmToken }) {
    try {
      const data = await this.fetch.post(
        '/notification/fcmtoken/createfcmtoken',
        {
          userId,
          token: fcmToken,
          deviceId: 'default',
          keySecret: 'abc',
        },
      );
      return data;
    } catch (error) {
      return false;
    }
  }
}

export default new ContactService();
