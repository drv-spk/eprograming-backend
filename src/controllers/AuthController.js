// import libs

// import handlers
import AuthHandler from '../handlers/AuthHandler';
// import services

// import Error Codes
import ErrorCodes from '../commons/ErrorCodes';
// import utils
import { generateToken, verifyToken } from '../utils/jwt';
// import types
import {} from '../commons/Types';
// import another type
import { KEY_AUTH } from '../config/Server';
// import error types
import UnauthorizedError from '../errors/unauthorized';
import ValidateError from '../errors/validation';
import AlreadyError from '../errors/already-exists';
import NotFoundError from '../errors/not-found';

class AuthController {
  async login(req, res) {
    const { email, password } = req.body;
    try {
      // validate input
      if (!email) {
        throw new ValidateError(ErrorCodes.EMAIL_IS_EMPTY);
      }
      if (!password) {
        throw new ValidateError(ErrorCodes.PASSWORD_IS_EMPTY);
      }
      let user = null;
      user = await AuthHandler.getUserByEmail(email);
      if (!user) {
        throw new NotFoundError(ErrorCodes.NOT_EXISTED_THIS_EMAIL);
      }
      // compare password
      const isMatchPassword = await user.comparePassword(password);
      if (!isMatchPassword) {
        throw new UnauthorizedError(ErrorCodes.INCORRECT_PASSWORD);
      }
      // create token
      const jwt = generateToken(AuthHandler.getPayloadJwtSchema(user));
      // return record user
      res.onSuccess({ jwt });
    } catch (error) {
      res.onError(error);
    }
  }

  async register(req, res) {
    const { email, password, role } = req.body;
    try {
      // Validator input
      if (!email) {
        throw new ValidateError(ErrorCodes.EMAIL_IS_EMPTY);
      }
      if (!password) {
        throw new ValidateError(ErrorCodes.PASSWORD_IS_EMPTY);
      } else if (password.length < 6) {
        throw new ValidateError(ErrorCodes.LENGTH_PASSWORD_LESS_THAN_6);
      }
      if (!role) {
        throw new ValidateError(ErrorCodes.ROLE_IS_EMPTY);
      }
      const user = await AuthHandler.getUserByEmail(email);
      if (user) {
        throw new AlreadyError(ErrorCodes.ALREADY_HAVE_THIS_USER);
      }
      const newUser = await AuthHandler.createNewUser({
        email,
        password,
        role,
      });
      const jwt = generateToken(AuthHandler.getPayloadJwtSchema(newUser));
      res.onSuccess({ jwt });
    } catch (error) {
      res.onError(error);
    }
  }

  async getUserInfo(req, res) {
    const token = req.headers[KEY_AUTH] || null;
    try {
      if (!token) {
        throw new UnauthorizedError(ErrorCodes.REQUIRE_ACCESS_TOKEN);
      }
      const payload = verifyToken(token);
      let user = null;
      user = await AuthHandler.getUserById(payload.userId);
      res.onSuccess({ email: user.email, role: user.role });
    } catch (error) {
      res.onError(error);
    }
  }
}

export default new AuthController();
