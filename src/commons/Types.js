export const ExampleType = {
  PROPS1: 'PROPS1',
  PROPS2: 'PROPS2',
  PROPS3: 'PROPS3',
};

// product status
export const ProductStatus = {
  IN_STOCK: 'IN_STOCK',
  OUT_STOCK: 'OUT_STOCK',
};
