import AuthController from '../../controllers/AuthController';

const router = require('express').Router();
// Post
router.post('/login', AuthController.login);
router.post('/register', AuthController.register);
// Put

// Get
router.get('/getUserInfo', AuthController.getUserInfo);
// router.get('/getUsers', AuthController.getUsers);

export default router;
